﻿/*
 * Created by SharpDevelop.
 * User: melf
 * Date: 10/06/2012
 * Time: 2:54 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

using NHibernate;

using MSDPredictorService.Data;

namespace MSDPredictorService.Repositories
{
    /// <summary>
    /// Description of UserSongHistoryRepository.
    /// </summary>
    public class UserSongHistoryRepository: IUserSongHistoryRepository
    {
        public User SaveUser(User user)
        {
            using(ISession session = NHibernate.NHibernateHelper.OpenSession())
            {
                using(ITransaction transaction = session.BeginTransaction())
                {
                    session.Save(user);
                    transaction.Commit();
                    return user;
                }
            }
        }
        
//        public User GetUser(int userId)
//        {
//            throw new NotImplementedException();
//        }
        
        public User GetUserBySourceUserId(string sourceUserId)
        {
            throw new NotImplementedException();
        }
        
        public Song SaveSong(Song song)
        {
            using(ISession session = NHibernate.NHibernateHelper.OpenSession())
            {
                using(ITransaction transaction = session.BeginTransaction())
                {
                    session.Save(song);
                    transaction.Commit();
                    return song;
                }
            }
        }
//        public Song GetSong(int SongId)
//        {
//            throw new NotImplementedException();
//        }
        
        public Song GetSongBySourceSongId(string sourceSongId)
        {
            throw new NotImplementedException();
        }
        
        public PlayCount SavePlayCount(PlayCount playCount)
        {
            using(ISession session = NHibernate.NHibernateHelper.OpenSession())
            {
                using(ITransaction transaction = session.BeginTransaction())
                {
                    session.Save(playCount);
                    transaction.Commit();
                    return playCount;
                }
            }
        }
        
//        public PlayCount[] GetPlayCountsForUser(int userId)
//        {
//            throw new NotImplementedException();
//        }
        
//        public PlayCount[] GetPlayCountsForSong(int songId)
//        {
//            throw new NotImplementedException();
//        }

       
    }
}
