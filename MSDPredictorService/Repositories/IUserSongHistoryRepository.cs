﻿/*
 * Created by SharpDevelop.
 * User: melf
 * Date: 5/06/2012
 * Time: 9:26 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

using MSDPredictorService.Data;

namespace MSDPredictorService.Repositories
{
    /// <summary>
    /// Description of IUserSongHistoryRepository.
    /// </summary>
    public interface IUserSongHistoryRepository
    {
        User SaveUser(User user);
//        User GetUser(int userId);
        User GetUserBySourceUserId(string sourceUserId);
        
        Song SaveSong(Song song);
//        Song GetSong(int SongId);
        Song GetSongBySourceSongId(string sourceSongId);
        
        PlayCount SavePlayCount(PlayCount playCount);
//        PlayCount[] GetPlayCountsForUser(int userId);
//        PlayCount[] GetPlayCountsForSong(int songId);
        
    }
}
