﻿/*
 * Created by SharpDevelop.
 * User: melf
 * Date: 10/06/2012
 * Time: 8:35 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

using NHibernate;
using NHibernate.Cfg;

using MSDPredictorService.Data;

namespace MSDPredictorService.Repositories.NHibernate
{
    /// <summary>
    /// Description of NHibernateHelper.
    /// </summary>
    public class NHibernateHelper
    {
        private static ISessionFactory _sessionFactory;
 
        private static ISessionFactory SessionFactory
        {
            get
            {
                if(_sessionFactory == null)
                {
                    var configuration = new Configuration();
                    configuration.Configure();
                    configuration.AddAssembly(typeof(Song).Assembly);
                    _sessionFactory = configuration.BuildSessionFactory();
                }
                return _sessionFactory;
            }
        }
 
        public static ISession OpenSession()
        {
            return SessionFactory.OpenSession();
        }
    }
}
