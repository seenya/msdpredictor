﻿/*
 * Created by SharpDevelop.
 * User: melf
 * Date: 5/06/2012
 * Time: 8:17 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.IO;

using MSDPredictorService.Data;
using MSDPredictorService.Readers;
using MSDPredictorService.Repositories;

namespace MSDPredictorService
{
	public interface IUserSongHistoryProcessor
	{
		void ImportUserSongPlayCounts(string fileName);
	}
}
