﻿/*
 * Created by SharpDevelop.
 * User: melf
 * Date: 5/06/2012
 * Time: 8:17 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.IO;

using MSDPredictorService.Data;
using MSDPredictorService.Readers;
using MSDPredictorService.Repositories;

namespace MSDPredictorService
{
	/// <summary>
	/// Description of UserSongHistoryProcessor.
	/// </summary>
	public class UserSongHistoryProcessor : IUserSongHistoryProcessor
	{
		private IUserSongHistoryRepository _repository;
		private Dictionary<string, User> _userCache = new Dictionary<string, User>();
		private Dictionary<string, Song> _songCache = new Dictionary<string, Song>();

		public UserSongHistoryProcessor(IUserSongHistoryRepository repository)
		{
			_repository = repository;
		}

		public void ImportUserSongPlayCounts(string fileName)
		{
			using (IUserSongHistoryReader reader = new UserSongHistoryReader(fileName, '\t')) {
				SourceTriplet sourceRecord = reader.GetNextRecord();
				while (sourceRecord != null) {
					SavePlayCount(sourceRecord);
					sourceRecord = reader.GetNextRecord();
				}
			}
		}

		private void SavePlayCount(SourceTriplet sourceRecord)
		{
			User theUser = GetUserBySourceUserId(sourceRecord.SourceUserId);
			Song theSong = GetSongBySourceSongId(sourceRecord.SourceSongId);
			PlayCount playCount = new PlayCount {
				UserId = theUser.UserId,
				SongId = theSong.SongId,
				Count = sourceRecord.PlayCount
			};
			_repository.SavePlayCount(playCount);
		}

		private User GetUserBySourceUserId(string sourceUserId)
		{
			if (!_userCache.ContainsKey(sourceUserId)) {
				_userCache[sourceUserId] = _repository.SaveUser(new User { SourceUserId = sourceUserId });
			}
			return _userCache[sourceUserId];
		}

		private Song GetSongBySourceSongId(string sourceSongId)
		{
			if (!_songCache.ContainsKey(sourceSongId)) {
				_songCache[sourceSongId] = _repository.SaveSong(new Song { SourceSongId = sourceSongId });
			}
			return _songCache[sourceSongId];
		}
	}
}
