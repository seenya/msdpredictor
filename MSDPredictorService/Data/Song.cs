﻿/*
 * Created by SharpDevelop.
 * User: melf
 * Date: 5/06/2012
 * Time: 9:24 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace MSDPredictorService.Data
{
    /// <summary>
    /// Description of Song.
    /// </summary>
    public class Song
    {
        public virtual int SongId { get; set; }
        public virtual string SourceSongId { get; set; }
    }
}
