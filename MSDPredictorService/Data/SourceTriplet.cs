﻿/*
 * Created by SharpDevelop.
 * User: melf
 * Date: 4/06/2012
 * Time: 8:24 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace MSDPredictorService.Data
{
    /// <summary>
    /// Description of SourceTriplet.
    /// </summary>
    public class SourceTriplet
    {
        
        public string SourceUserId { get; set; }
        
        public string SourceSongId { get; set; }
        
        public int PlayCount { get; set; }
        
        public string ToSourceString(char delimeter)
        {
            return string.Format("{0}{3}{1}{3}{2}", this.SourceUserId, this.SourceSongId, this.PlayCount, delimeter);
        }
        
        
    }
}
