﻿/*
 * Created by SharpDevelop.
 * User: melf
 * Date: 5/06/2012
 * Time: 9:24 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace MSDPredictorService.Data
{
    /// <summary>
    /// Description of User.
    /// </summary>
    public class User
    {
        public virtual int UserId { get; set; }
        public virtual string SourceUserId { get; set; }
    }
}
