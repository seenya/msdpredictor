﻿/*
 * Created by SharpDevelop.
 * User: Naes
 * Date: 31/05/2012
 * Time: 10:32 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace MSDPredictorService.Data
{
    using System;
    
    /// <summary>
    /// Description of PlayRecord.
    /// </summary>
    public class PlayCount
    {
        public virtual int PlayCountId { get; set; }
        
        public virtual int UserId { get; set; }
        
        public virtual int SongId { get; set; }
        
        public virtual int Count { get; set; }
    }
}
