﻿/*
 * Created by SharpDevelop.
 * User: Naes
 * Date: 31/05/2012
 * Time: 10:19 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

using MSDPredictorService.Data;

namespace MSDPredictorService.Readers
{
    public interface IUserSongHistoryReader:IDisposable 
    {
        SourceTriplet GetNextRecord();
    }
}
