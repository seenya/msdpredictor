﻿/*
 * Created by SharpDevelop.
 * User: Naes
 * Date: 31/05/2012
 * Time: 10:19 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;

using MSDPredictorService.Data;

namespace MSDPredictorService.Readers
{
    /// <summary>
    /// Description of MSDHistoryRepository.
    /// </summary>
    public class UserSongHistoryReader : IUserSongHistoryReader
    {
        // Not currently used.
        // private const char DEFAULT_DELIMETER = ';';
        private const int USER_ID_INDEX = 0;
        private const int SONG_ID_INDEX = 1;
        private const int PLAY_COUNT_INDEX = 2;
        
        private bool _disposed = false;
        private char _delimeter;
        private int _lineNumber = 0;
        private StreamReader _sourceStream;
        
        public UserSongHistoryReader(string fileName, char fieldDelimeter):this(new StreamReader(fileName), fieldDelimeter)
        {
        }
        
        public UserSongHistoryReader(StreamReader sourceStream, char fieldDelimeter)
        {
            _delimeter = fieldDelimeter;
            _sourceStream = sourceStream;
        }

        public SourceTriplet GetNextRecord()
        {
            _lineNumber ++;
            
            if(_lineNumber % 1000 == 0)
                System.Diagnostics.Debug.WriteLine(string.Format("Reading line number {0}", _lineNumber));

            string line = _sourceStream.ReadLine();
            if(line == null)
                return null;
            
            string [] parts = line.Split(_delimeter);
            if(parts.Length != 3)
            {
                throw new ArgumentException(string.Format("Expected 3 fields, but there were only {0} at line {1}", parts.Length, _lineNumber));
            }
            
            SourceTriplet triplet = new SourceTriplet();
            triplet.SourceUserId = GetSourceUserId(parts);
            triplet.SourceSongId = GetSourceSongId(parts);
            triplet.PlayCount = GetSourcePlayCount(parts);

            return triplet;
        }

        private int GetSourcePlayCount(string[] parts)
        {
            string playCountAsString = parts[PLAY_COUNT_INDEX];
            int playCount = 0;
            if (!int.TryParse(playCountAsString, out playCount)) 
            {
                throw new ArgumentException(String.Format("Play Count '{0}' is not numeric at line {1}", playCountAsString, _lineNumber));
            }
            return playCount;
        }
        
        private string GetSourceUserId(string [] parts)
        {
            string sourceUserId = parts[USER_ID_INDEX];
            if(string.IsNullOrEmpty(sourceUserId))
            {
                throw new ArgumentException(String.Format("User Id was not supplied at line {0}", _lineNumber));
            }
            return sourceUserId;
        }
        
        private string GetSourceSongId(string [] parts)
        {
            string sourceSongId = parts[SONG_ID_INDEX];
            if(string.IsNullOrEmpty(sourceSongId))
            {
                throw new ArgumentException(String.Format("Song Id was not supplied at line {0}", _lineNumber));
            }
            return sourceSongId;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
        protected virtual void Dispose(bool disposing)
        {
            if(!this._disposed)
            {
                if(disposing)
                {
                    _sourceStream.Dispose();
                }
                
                _disposed = true;
            }
        }
    }
}
