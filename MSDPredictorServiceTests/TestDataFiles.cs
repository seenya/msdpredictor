﻿/*
 * Created by SharpDevelop.
 * User: melf
 * Date: 6/06/2012
 * Time: 9:03 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace MSDPredictorServiceTests
{
    /// <summary>
    /// Description of TestDataFiles.
    /// </summary>
    public class TestDataFiles
    {
        public const string FILE_WITH_2_VALID_RECORDS_FOR_ONE_USER = "./Data/2ValidRecordsForSameUser.txt";
        public const string FILE_WITH_2_VALID_RECORDS_FOR_DIFFERENT_USERS_SAME_SONG = "./Data/2ValidRecordsForDifferentUsersButSameSong.txt";
        public const string FILE_WITH_2_VALID_RECORDS_FOR_DIFFERENT_USERS = "./Data/2ValidRecordsForDifferentUsers.txt";
 
    }
}
