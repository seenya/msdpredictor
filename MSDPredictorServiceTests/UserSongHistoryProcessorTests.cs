﻿/*
 * Created by SharpDevelop.
 * User: melf
 * Date: 5/06/2012
 * Time: 8:22 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

using Moq;
using NUnit.Framework;

using MSDPredictorService;
using MSDPredictorService.Data;
using MSDPredictorService.Repositories;

namespace MSDPredictorServiceTests
{
    [TestFixture]
    public class UserSongHistoryProcessorTests
    {
       
        [Test]
        public void ImportUserSongPlayCounts_MultipleRecordsForSameSourceUserId_SaveUserOnlyCalledOnce()
        {
            int saveUserCount = 0;
            int saveSongCount = 0;
            int savePlayCountCount = 0;
            Mock<IUserSongHistoryRepository> mockRepo = new Mock<IUserSongHistoryRepository>();
            mockRepo.Setup(x => x.SaveUser(It.IsAny<User>())).Returns((User u) => new User() { UserId = saveUserCount++, SourceUserId = u.SourceUserId });
            mockRepo.Setup(x => x.SaveSong(It.IsAny<Song>())).Returns((Song s) => new Song() { SongId = saveSongCount++, SourceSongId = s.SourceSongId });
            mockRepo.Setup(x => x.SavePlayCount(It.IsAny<PlayCount>())).Returns((PlayCount p) => new PlayCount() { Count = savePlayCountCount++, UserId = p.UserId, SongId = p.SongId });
            
            UserSongHistoryProcessor processor = new UserSongHistoryProcessor(mockRepo.Object);
            processor.ImportUserSongPlayCounts(TestDataFiles.FILE_WITH_2_VALID_RECORDS_FOR_ONE_USER);
            
            Assert.AreEqual(1, saveUserCount, "Save User should be called exactly once.");
        }

        [Test]
        public void ImportUserSongPlayCounts_MultipleRecordsForDifferentSourceUserId_SaveUserCalledTwice()
        {
            int saveUserCount = 0;
            int saveSongCount = 0;
            int savePlayCountCount = 0;
            Mock<IUserSongHistoryRepository> mockRepo = new Mock<IUserSongHistoryRepository>();
            mockRepo.Setup(x => x.SaveUser(It.IsAny<User>())).Returns((User u) => new User() { UserId = saveUserCount++, SourceUserId = u.SourceUserId });
            mockRepo.Setup(x => x.SaveSong(It.IsAny<Song>())).Returns((Song s) => new Song() { SongId = saveSongCount++, SourceSongId = s.SourceSongId });
            mockRepo.Setup(x => x.SavePlayCount(It.IsAny<PlayCount>())).Returns((PlayCount p) => new PlayCount() { Count = savePlayCountCount++, UserId = p.UserId, SongId = p.SongId });
            
            UserSongHistoryProcessor processor = new UserSongHistoryProcessor(mockRepo.Object);
            processor.ImportUserSongPlayCounts(TestDataFiles.FILE_WITH_2_VALID_RECORDS_FOR_DIFFERENT_USERS);
            
            Assert.AreEqual(2, saveUserCount, "Save User should be called exactly twice.");
        }

        [Test]
        public void ImportUserSongPlayCounts_MultipleRecordsForSameSourceSongId_SaveSongOnlyCalledOnce()
        {
            int saveUserCount = 0;
            int saveSongCount = 0;
            int savePlayCountCount = 0;
            Mock<IUserSongHistoryRepository> mockRepo = new Mock<IUserSongHistoryRepository>();
            mockRepo.Setup(x => x.SaveUser(It.IsAny<User>())).Returns((User u) => new User() { UserId = saveUserCount++, SourceUserId = u.SourceUserId });
            mockRepo.Setup(x => x.SaveSong(It.IsAny<Song>())).Returns((Song s) => new Song() { SongId = saveSongCount++, SourceSongId = s.SourceSongId });
            mockRepo.Setup(x => x.SavePlayCount(It.IsAny<PlayCount>())).Returns((PlayCount p) => new PlayCount() { Count = savePlayCountCount++, UserId = p.UserId, SongId = p.SongId });
            
            UserSongHistoryProcessor processor = new UserSongHistoryProcessor(mockRepo.Object);
            processor.ImportUserSongPlayCounts(TestDataFiles.FILE_WITH_2_VALID_RECORDS_FOR_DIFFERENT_USERS_SAME_SONG);
            
            Assert.AreEqual(1, saveSongCount, "Save Song should be called exactly once.");
        }

        [Test]
        public void ImportUserSongPlayCounts_2Records_SavePlayCountCalledTwice()
        {
            int saveUserCount = 0;
            int saveSongCount = 0;
            int savePlayCountCount = 0;
            Mock<IUserSongHistoryRepository> mockRepo = new Mock<IUserSongHistoryRepository>();
            mockRepo.Setup(x => x.SaveUser(It.IsAny<User>())).Returns((User u) => new User() { UserId = saveUserCount++, SourceUserId = u.SourceUserId });
            mockRepo.Setup(x => x.SaveSong(It.IsAny<Song>())).Returns((Song s) => new Song() { SongId = saveSongCount++, SourceSongId = s.SourceSongId });
            mockRepo.Setup(x => x.SavePlayCount(It.IsAny<PlayCount>())).Returns((PlayCount p) => new PlayCount() { Count = savePlayCountCount++, UserId = p.UserId, SongId = p.SongId });
            
            UserSongHistoryProcessor processor = new UserSongHistoryProcessor(mockRepo.Object);
            processor.ImportUserSongPlayCounts(TestDataFiles.FILE_WITH_2_VALID_RECORDS_FOR_DIFFERENT_USERS_SAME_SONG);
            
            Assert.AreEqual(2, savePlayCountCount, "Save Play Count should be called exactly twice.");
        }
    }
}
