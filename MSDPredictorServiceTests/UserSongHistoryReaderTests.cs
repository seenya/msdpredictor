﻿/*
 * Created by SharpDevelop.
 * User: Naes
 * Date: 31/05/2012
 * Time: 10:03 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.IO;

using Moq;
using NUnit.Framework;

using MSDPredictorService.Data;
using MSDPredictorService.Readers;

namespace MSDPredictorServiceTests
{

    /// <summary>
    /// Description of MyClass.
    /// </summary>
    [TestFixture]
    public class UserSongHistoryReaderTests
    {
        private const char DELIMETER = '\t';
        
        [Test]
        public void GetNextRecord_NoRecords_ReturnsNothing()
        {
            Mock<StreamReader> mockStream = new Mock<StreamReader>(new object[] { TestDataFiles.FILE_WITH_2_VALID_RECORDS_FOR_DIFFERENT_USERS });
            mockStream.Setup(x => x.ReadLine()).Returns(() => null);
            
            using (UserSongHistoryReader reader = new UserSongHistoryReader(mockStream.Object, DELIMETER))
            {
                SourceTriplet record = reader.GetNextRecord();
                Assert.IsNull(record);
            }
        }
        
        [Test]
        public void GetNextRecord_OneRecord_ReturnsOneRecord()
        {
            SourceTriplet original = new SourceTriplet() { SourceUserId = "b80344d063b5ccb3212f76538f3d9e43d87dca9e", SourceSongId = "SOAKIMP12A8C130995", PlayCount = 1 };
            
            Mock<StreamReader> mockStream = new Mock<StreamReader>(new object[] { TestDataFiles.FILE_WITH_2_VALID_RECORDS_FOR_DIFFERENT_USERS });
            mockStream.Setup(x => x.ReadLine()).Returns(original.ToSourceString(DELIMETER));
            using (UserSongHistoryReader reader = new UserSongHistoryReader(mockStream.Object, DELIMETER))
            {
                SourceTriplet record = reader.GetNextRecord();
                AssertSourceTripletsAreEqual(original, record);
            }
        }
        
        [Test, ExpectedException(typeof(ArgumentException))]
        public void GetNextRecord_OneRecordWithNonNumericPlayCount_ExceptionThrown()
        {
            SourceTriplet original = new SourceTriplet() { SourceUserId = "b80344d063b5ccb3212f76538f3d9e43d87dca9e", SourceSongId = "SOAKIMP12A8C130995", PlayCount = 1 };
            
            Mock<StreamReader> mockStream = new Mock<StreamReader>(new object[] { TestDataFiles.FILE_WITH_2_VALID_RECORDS_FOR_DIFFERENT_USERS });
            mockStream.Setup(x => x.ReadLine()).Returns(original.ToSourceString(DELIMETER) + "a");
            using (UserSongHistoryReader reader = new UserSongHistoryReader(mockStream.Object, DELIMETER))
            {
                SourceTriplet record = reader.GetNextRecord();
            }
        }
        
        [Test, ExpectedException(typeof(ArgumentException))]
        public void GetNextRecord_OneRecordWithEmptyUserSourceId_ExceptionThrown()
        {
            SourceTriplet original = new SourceTriplet() { SourceUserId = string.Empty, SourceSongId = "SOAKIMP12A8C130995", PlayCount = 1 };
            
            Mock<StreamReader> mockStream = new Mock<StreamReader>(new object[] { TestDataFiles.FILE_WITH_2_VALID_RECORDS_FOR_DIFFERENT_USERS });
            mockStream.Setup(x => x.ReadLine()).Returns(original.ToSourceString(DELIMETER));
            using (UserSongHistoryReader reader = new UserSongHistoryReader(mockStream.Object, DELIMETER))
            {
                SourceTriplet record = reader.GetNextRecord();
            }
        }
        
        [Test, ExpectedException(typeof(ArgumentException))]
        public void GetNextRecord_OneRecordWithOnly1Tab_ExceptionThrown()
        {
            Mock<StreamReader> mockStream = new Mock<StreamReader>(new object[] { TestDataFiles.FILE_WITH_2_VALID_RECORDS_FOR_DIFFERENT_USERS });
            mockStream.Setup(x => x.ReadLine()).Returns("userid\tsongid");
            using (UserSongHistoryReader reader = new UserSongHistoryReader(mockStream.Object, DELIMETER))
            {
                SourceTriplet record = reader.GetNextRecord();
            }
        }
        
        [Test]
        public void GetNextRecord_SourceFileHas2Entries_Only2RecordsFoundAndDataIsAsExpected()
        {
            SourceTriplet original1 = new SourceTriplet() { SourceUserId = "b80344d063b5ccb3212f76538f3d9e43d87dca9e", SourceSongId = "SOAKIMP12A8C130995", PlayCount = 1 };
            SourceTriplet original2 = new SourceTriplet() { SourceUserId = "b80344d063b5ccb3212f76538f3d9e43d87dca9d", SourceSongId = "DOAPDEY12A81C210A9", PlayCount = 3 };
            
            using (StreamReader innerReader = new StreamReader(TestDataFiles.FILE_WITH_2_VALID_RECORDS_FOR_DIFFERENT_USERS))
            {
                using (UserSongHistoryReader reader = new UserSongHistoryReader(innerReader, DELIMETER))
                {
                    SourceTriplet record = reader.GetNextRecord();
                    AssertSourceTripletsAreEqual(original1, record);
                    record = reader.GetNextRecord();
                    AssertSourceTripletsAreEqual(original2, record);
                    record = reader.GetNextRecord();
                    Assert.IsNull(record);
                }
            }
        }
        
        private void AssertSourceTripletsAreEqual(SourceTriplet expected, SourceTriplet actual)
        {
            Assert.AreEqual(expected.SourceUserId, actual.SourceUserId, "UserSourceId does not match");
            Assert.AreEqual(expected.SourceSongId, actual.SourceSongId, "SongSourceId does not match");
            Assert.AreEqual(expected.PlayCount, actual.PlayCount, "PlayCount does not match");
        }
    }
}