﻿/*
 * Created by SharpDevelop.
 * User: melf
 * Date: 10/06/2012
 * Time: 8:30 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

using NUnit.Framework;

using MSDPredictorService.Data;
using MSDPredictorService.Repositories;

namespace MSDPredictorServiceTests
{
    /// <summary>
    /// Description of UserSongHistoryRepositoryTests.
    /// </summary>
    [TestFixture]
    public class UserSongHistoryRepositoryTests
    {
        
        [Test]
        public void AddSong_ValidSong_IsPersistedAndIdFieldIsPopulated()
        {
            Song theSong = new Song() { SourceSongId="MySourceSongId" };
            IUserSongHistoryRepository repo = new UserSongHistoryRepository();
            
            Song savedSong = repo.SaveSong(theSong);
            
            Assert.IsTrue(savedSong.SongId > 0);
        }
        
    }
}
