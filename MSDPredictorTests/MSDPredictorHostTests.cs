﻿/*
 * Created by SharpDevelop.
 * User: melf
 * Date: 11/06/2012
 * Time: 1:47 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

using NUnit.Framework;
using Moq;

using MSDPredictor;
using MSDPredictorService;

namespace MSDPredictorTests
{
    /// <summary>
    /// Description of MSDPredictorHostTests.
    /// </summary>
    [TestFixture()]
    public class MSDPredictorHostTests
    {
        private const int RETURN_CODE_ERROR = 1;
        private const int RETURN_CODE_SUCCESS = 0;
        
        private const string ARG_IMPORT = "Import";
        private const string ARG_FILENAME = "FileName";
        
        [Test]
        public void Run_NoArgumentsProvided_ReturnCodeIndicatesError()
        {
            var mockConsole = new Mock<IConsoleWriter>();
            var mockHistoryProcessor = new Mock<IUserSongHistoryProcessor>();
            MSDPredictorHost host = new MSDPredictorHost(mockHistoryProcessor.Object, mockConsole.Object);
            string [] args = {};
            
            int returnValue = host.Run(args);
            
            Assert.AreEqual(RETURN_CODE_ERROR, returnValue);
        }
        
        [Test]
        public void Run_ImportArgumentProvidedOnly_ReturnCodeIndicatesError()
        {
            var mockConsole = new Mock<IConsoleWriter>();
            var mockHistoryProcessor = new Mock<IUserSongHistoryProcessor>();
            MSDPredictorHost host = new MSDPredictorHost(mockHistoryProcessor.Object, mockConsole.Object);
            string [] args = {ARG_IMPORT};
            
            int returnValue = host.Run(args);
            
            Assert.AreEqual(RETURN_CODE_ERROR, returnValue);
        }
        
        [Test]
        public void Run_ImportArgumentAndFileNameProvided_ReturnCodeIndicatesSuccessAndHistoryProcessorImportCalled()
        {
            string myFileName = "myfile.txt";
            
            var mockConsole = new Mock<IConsoleWriter>();
            var mockHistoryProcessor = new Mock<IUserSongHistoryProcessor>();
            mockHistoryProcessor.Setup(m =>  m.ImportUserSongPlayCounts(myFileName));
            
            MSDPredictorHost host = new MSDPredictorHost(mockHistoryProcessor.Object, mockConsole.Object);
            string [] args = {ARG_IMPORT, String.Format("{0}={1}", ARG_FILENAME, myFileName)};
            
            int returnValue = host.Run(args);
            
            Assert.AreEqual(RETURN_CODE_SUCCESS, returnValue);
            mockHistoryProcessor.Verify(m => m.ImportUserSongPlayCounts(myFileName), Times.Once());
        }
        
        [Test]
        public void Run_ImportArgumentAndFileNameProvided_ReturnCodeIndicatesError()
        {
            string myFileName = "myfile.txt";
             
            var mockConsole = new Mock<IConsoleWriter>();
            var mockHistoryProcessor = new Mock<IUserSongHistoryProcessor>();
            
            MSDPredictorHost host = new MSDPredictorHost(mockHistoryProcessor.Object, mockConsole.Object);
            string [] args = {ARG_IMPORT, String.Format("{0}:{1}", ARG_FILENAME, myFileName)};
            
            int returnValue = host.Run(args);
            
            Assert.AreEqual(RETURN_CODE_ERROR, returnValue);
        }
    }
}
