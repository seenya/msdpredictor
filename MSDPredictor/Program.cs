﻿/*
 * Created by SharpDevelop.
 * User: melf
 * Date: 11/06/2012
 * Time: 1:28 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

using StructureMap;

namespace MSDPredictor
{
    class Program
    {
        public static int Main(string[] args)
        {
            BootstrapStructureMap();
            IMSDPredictorHost host = ObjectFactory.GetInstance<IMSDPredictorHost>();
            return host.Run(args);
        }
        
        private static void BootstrapStructureMap()
        {
            
            ObjectFactory.Initialize(x => 
            {
                x.Scan(o =>
                {
                    o.Assembly(typeof(MSDPredictorService.UserSongHistoryProcessor).Assembly);
                    o.Assembly(typeof(MSDPredictorHost).Assembly);
                    o.WithDefaultConventions();
                });
            });
        }
    }
}