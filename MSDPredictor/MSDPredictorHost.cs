﻿/*
 * Created by SharpDevelop.
 * User: melf
 * Date: 11/06/2012
 * Time: 1:29 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

using MSDPredictorService;

namespace MSDPredictor
{
	/// <summary>
	/// Description of MSDPredictorHost.
	/// </summary>
	public class MSDPredictorHost : IMSDPredictorHost
	{
		private const int RETURN_VALUE_SUCCESS = 0;
		private const int RETURN_VALUE_ERROR = 1;

		private const string ARG_HELP = "?";
		private const string ARG_IMPORT = "import";
		private const string ARG_FILENAME = "filename";
		private const string ARG_VALUE_SEPARATOR = "=";

		private IUserSongHistoryProcessor _userSongHistoryProcessor;
		private IConsoleWriter _consoleWriter;


		public MSDPredictorHost(IUserSongHistoryProcessor userSongHistoryProcessor, IConsoleWriter consoleWriter)
		{
			_userSongHistoryProcessor = userSongHistoryProcessor;
			_consoleWriter = consoleWriter;
		}

		public int Run(string[] args)
		{
			try 
			{
				if (args.Length == 0) 
				{
					throw new ArgumentException("No arguments supplied");
			    }
				
				string action = args[0].ToLower();
				if(action == ARG_IMPORT)
				{
				    if(args.Length != 2)
				    {
				        throw new ArgumentException(String.Format("Action {0} expects the argument {1}", ARG_IMPORT, ARG_FILENAME));
				    }
				    
				    string fileName = GetParameterValueFromString(args[1]);
				    
				    _userSongHistoryProcessor.ImportUserSongPlayCounts(fileName);
				}

				return RETURN_VALUE_SUCCESS;
			} 
			catch (Exception ex) 
			{
				PrintHelpToConsole(ex.Message);
				return RETURN_VALUE_ERROR;
			}
		}
		
		private string GetParameterValueFromString(string parameterString)
		{
		    int index = parameterString.IndexOf(ARG_VALUE_SEPARATOR);
		    if(index < 0)
		    {
		        throw new ArgumentException(String.Format("There was no value supplied in the parameter '{0}', remember that values are set to parameters uing '{1}'.", parameterString, ARG_VALUE_SEPARATOR));
		    }
		    
		    // Increment index to ensure separator charactor is not included.
		    index++;
		    return parameterString.Substring(index);
		}

		private void PrintHelpToConsole(string firstLineOfContent)
		{
			_consoleWriter.WriteLine(firstLineOfContent);
			_consoleWriter.WriteLine();
			_consoleWriter.WriteLine(String.Format("MSDPredictor expects arguments in the format 'exe {0} {1}=<nameOfFile>.", ARG_IMPORT, ARG_FILENAME));
		}
	}
}
