﻿/*
 * Created by SharpDevelop.
 * User: melf
 * Date: 11/06/2012
 * Time: 1:29 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

using MSDPredictorService;

namespace MSDPredictor
{
	public interface IMSDPredictorHost
	{
		int Run(string[] args);
	}
}
