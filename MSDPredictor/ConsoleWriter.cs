﻿/*
 * Created by SharpDevelop.
 * User: melf
 * Date: 11/06/2012
 * Time: 1:44 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace MSDPredictor
{
    /// <summary>
    /// Description of ConsoleWriter.
    /// </summary>
    public class ConsoleWriter:IConsoleWriter
    {
        public void WriteLine()
        {
            Console.WriteLine();
        }
        
        public void WriteLine(string line)
        {
            Console.WriteLine(line);
        }
    }
}
