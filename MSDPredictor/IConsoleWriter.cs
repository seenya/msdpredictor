﻿/*
 * Created by SharpDevelop.
 * User: melf
 * Date: 11/06/2012
 * Time: 1:43 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace MSDPredictor
{
    /// <summary>
    /// Description of IConsoleWriter.
    /// </summary>
    public interface IConsoleWriter
    {
        void WriteLine();
        void WriteLine(string line);
    }
}
